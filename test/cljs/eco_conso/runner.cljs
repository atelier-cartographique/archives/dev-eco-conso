(ns eco-conso.runner
    (:require [doo.runner :refer-macros [doo-tests]]
              [eco-conso.core-test]
              [eco-conso.table-test]))

(doo-tests 'eco-conso.core-test)
(doo-tests 'eco-conso.table-test)
