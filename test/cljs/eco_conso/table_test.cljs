(ns eco-conso.table-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [eco-conso.table :as table]))

(deftest px-test
  (testing "make a pixel"
    (is (= "32px" (table/px 32)))))

(deftest minmax-test
  (testing "min & max"
  (let [data '(
                {:product :coton-bio, :parameter 3, :value {:value 2, :label "x"}}
                {:product :bambou, :parameter 3, :value {:value 3, :label "v"}}
                {:product :chanvre, :parameter 3, :value {:value 4, :label "v"}}
                {:product :microfibre, :parameter 3, :value {:value 8, :label "v"}}
                )]
    (is (= {:min 2 :max 8} (table/min&max-val data))))))
