(ns eco-conso.css-popup
  (:require
    [garden.core :refer [css]]
    [garden.selectors :refer [after before]]
    [garden.stylesheet :refer [at-import at-media rgb]]
    [garden.def :as gdef]
    [garden.units :as u]
    [garden.color :as color]
    ))

;blanc
(def blanc (color/rgb 255 255 255))
; vert
(def vert-claire (color/rgb 0x98 0xce 0x1d))
(def vert (color/rgb 0x5e 0xa1 0x29))
(def vert-foncé (color/rgb 0x27 0x80 0x2c))
(def black " #000000 ")
(def green " #92D050 ")
(def greenHeader " #98C21D ")
(def grey " #817874 ")
(def white " white ")
(def purple " #8C3A8E")


(gdef/defcssfn url
  [unquoted-url]
  (str "\"" unquoted-url "\""))

; widget header

(defn style
  []
  [:#popup-view {
    :position "fixed"
    :height 0
    :right 0
    :bottom 0
    :left 0
    }
    [:.popup-mask {
      :position "absolute"
      :top 0
      :right 0
      :bottom 0
      :left 0
      }
        [:.popup-inner {
          :position "absolute"
          :height "auto"
          :right 0
          :bottom 0
          :width (u/px 424)
          :background-color green
          :padding (u/px 32)
          }
          [:.popup-close {
            :float "right"
            :cursor "pointer"
            :color white
            :font-size (u/px 32)
            :font-weight "bold"
            }]
          [:h1 {
            :color white
            :font-size (u/px 36)
            :line-height (u/px 36)
            :margin-bottom (u/px 24)
            }]]
      (at-media  [{:max-width (u/px 767)}]
        [:.popup-inner {
          :position "absolute"
          :height "auto"
          :right 0
          :bottom 0
          :left 0
          :background-color green
          :padding-right (u/px 80)
          }
          ])]])
