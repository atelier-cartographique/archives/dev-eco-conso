(ns eco-conso.css
  (:require
    [garden.core :refer [css]]
    [garden.def :refer [defstyles]]
    [garden.units :as u]
    [garden.selectors :refer [after before]]
    [garden.stylesheet :refer [at-import at-media rgb]]
    [eco-conso.css-table :as css-table]
    [eco-conso.css-popup :as css-popup]
    ))

(defn media-mobile [& rules]
   "ma fonction pour breakpoints screens"
   ;(at-media (str "max-width(" mobile-bkp ")") rules)
   (at-media  [{:max-width (u/px 767)}] rules)
   )

(def black " #000000 ")
(def green " #92D050 ")
(def greenHeader " #98C21D ")
(def grey " #817874 ")
(def white " white ")
(def purple " #8C3A8E")

(def big-title (u/percent 250))
(def medium-title (u/percent 200))

(def default-border 2)
(def border-green (str (/ default-border 2) "px solid" green))
(def border-black (str (/ default-border 2) "px solid" black))

(def default-margin 10)
(def margin (str default-margin "px"))
(def margin2 (str (* default-margin 2) "px"))
(def margin3 (str (* default-margin 3) "px"))
(def margin4 (str (* default-margin 4) "px"))

(def cW 70)
(def contentW (str cW "vw"))

(def lW 25)
(def leftW (str lW "vw"))

(def rW (- cW lW))
(def rightW (str rW "vw"))

(defstyles screen
    [
    (at-import "reset.css")
    (at-import "fonts.css")
        [:body {
            :font-family "'lato'"
            :font-size (u/percent 125)
            :line-height (u/percent 125)
        }]
        [:.trigger-popup {
          :cursor "pointer"
          :color purple
          :font-weight "bold"
          :border-bottom-width (u/px 1)
          :border-bottom-style "solid"
          }]
        [:p {:margin-bottom margin}]

        ; [:h1 {:font-size big-title}]
        ; [:h2 {:font-size medium-title}]

        [:ul {
            ; :margin (str margin2 " " 0)
        }
            [:li {:margin-bottom margin}
                [:&:before {
                    :content "'BD'"
                    :width (u/px 10)
                    :height (u/px 10)
                    :font-family "'ecoconsopicto'"
                    :color grey
                    :font-size (u/px 7)
                    :vertical-align "middle"
                    :margin-left (u/px -10)
                    :margin-right (u/px 5)
                    }
                ]
            ]
        ]

        [:ol {:counter-reset "li"} ;initiate the counter
            [:li {
                :list-style-type "none"
                ; :margin-left margin2
                :margin-top margin2
            }
                [:&.count:before {
                    :content  "counter(li)" ;apply counter and styles
                    :counter-increment "li"
                    :font-family "'oswaldlight'"
                    :color green
                    :margin-right margin
                    }]
                [:li {
                    :list-style-type "none"
                    :margin (str 0 " " 0)
                    }]
            ]
        ]

        [:hr {
            :border 0
            :height (u/px 1)
            :background-color green
        }]

        [:.content {
            :width contentW
            :margin-left "10vw"
            :box-sizing "border-box"
        }]

        [:article {
            :border-top border-green
            :display "flex"
            :padding (str margin2 " " 0)}]

        [:header {
            :color white
            :background-color greenHeader
            :margin-bottom margin}

            [:.header-top {
                :background-color greenHeader
                :height (u/px 122)}

                [:.content {
                    :display "flex"
                    :height (u/percent 100)
                    :align-items "center"}

                    [:.logo {
                        :flex (str 0 " " leftW)
                        :padding-right margin3}

                        [:img {
                            :height (u/px 72)
                            }]
                        ]

                    [:.slogan {
                        :flex 1}
                        [:span {
                            :display "inline-block"
                            }]
                        ]

                    [:.search {
                        :flex (str 0 " 185px")
                        :margin-right margin}]

                    [:.menu {:flex 0}]
                ]

                [:.logo {:text-align "right"}]

                [:.search {}

                    [:input {
                        :background "transparent"
                        :border 0
                        :border-bottom "1px solid #fff"
                        :color "#fff"
                        :width (u/px 160)
                        }
                        [:&:focus{
                            :outline "none"
                        }]
                    ]
                    [:&:after {
                        :content "''"
                        :width (u/px 20)
                        :height (u/px 25)
                        :background "url('pictos/picto-search.svg') center no-repeat"
                        :position "absolute"
                        :background-size "contain"
                        }
                    ]

                    [:&(str "input::-webkit-input-placeholder") {
                        :color "#fff"
                        :font-family "'lato'"
                        :font-size (u/percent 150)}]
                    [:&(str "input:-webkit-input-placeholder") {
                        :color "#fff"
                        :font-family "'lato'"
                        :font-size (u/percent 150)}]
                    [:&(str "input::-moz-input-placeholder") {
                        :color "#fff"
                        :font-family "'lato'"
                        :font-size (u/percent 150)}]
                    [:&(str "input:-ms-input-placeholder") {
                        :color "#fff"
                        :font-family "'lato'"
                        :font-size (u/percent 150)}]

                ]
            ]
            [:.header-bottom {
                :height (u/px 190)
                :background-size "cover"
                :background-color "#7BB9E0"
                :background-repeat "no-repeat"
                :background-position "center"
            }
                [:&.header-lange {:background-image "url('imgs/header-back-lange.jpg')"}]
                [:&.header-sechelinge {:background-image "url('imgs/header-back-sechelinge.jpg')"}]
                [:&.header-insecticide {:background-image "url('imgs/header-back-insecticide.jpg')"}]

                [:.content {
                    :display "flex"
                    :height (u/percent 100)
                    :align-items "center"}

                    [:.title {:flex (str 0 " " leftW)}]
                    [:.sub-title {:flex (str 0 " " rightW)}]
                ]

                [:.title {
                    :text-transform "uppercase"
                    :padding-right margin3
                    :text-align "right"
                    :line-height (u/percent 100)
                    :font-size medium-title
                    :font-family "'news_cycle'"

                }]
                [:.sub-title {
                    :font-family "'oswaldlight'"
                    :font-size big-title
                    :line-height (u/percent 100)
                    }]
            ]
        ]

        [:.left-side {
            :flex leftW
            ; :background-color "#ccc"
            :font-family "'oswaldlight'"
            :text-align "right"
            :color grey
            :padding-right margin3
            }

            [:.left-number {
                :vertical-align "super"
                :font-size (u/percent 80)
                }]

            [:.left-title {
                :font-size (u/percent 175)
                :font-weight 400
                :position "relative"
                :color black
                :line-height (u/percent 100)
                }
                [:&:after {
                    ; :content "''"
                    ; :left 0
                    ; :position "absolute"
                    ; :right 0
                    ; :bottom "15%"
                    ; :border-bottom border-black;
                    }
                ]

                [:&.keyword {
                    :font-size (u/px 26)
                    :line-height (u/percent 110)
                    :color purple
                }
                ]
            ]
        ]

        [:.right-side {
            :flex rightW
            ; :padding-top margin
            }

            ;; not the best place though
            [:.eco-table-container {
                :margin-top (u/px 32)
                :margin-bottom (u/px 32)
              }]

            [:.content-left {
                :position "relative"
                :width leftW
                :left (str "-" leftW)
                :margin-left (str "-" margin2)
                :margin-right (str "-" leftW)
                :text-align "right"
                :float "left"
                }
                [:.picto-eviter:before {
                    :content "' '"
                    :background "url('pictos/picto-eviter.svg') center no-repeat"
                    :width (u/px 40)
                    :height (u/px 28)
                    :display "inline-block"
                    :background-size "contain"
                    }]
                [:.picto-default:before {
                    :content "' '"
                    :background "url('pictos/picto-default.svg') center no-repeat"
                    :width (u/px 40)
                    :height (u/px 28)
                    :display "inline-block"
                    :background-size "contain"
                    }]
                [:.picto-conseil:before {
                    :content "' '"
                    :background "url('pictos/picto-conseil.svg') center no-repeat"
                    :width (u/px 40)
                    :height (u/px 28)
                    :display "inline-block"
                    :background-size "contain"
                    }]
            ]

            [:.title {
                :font-family "'oswaldlight'"
                :font-size (u/px 28)
                :padding-bottom margin2
                :padding-top margin2
                :color grey
                :line-height (u/percent 100)
                }]

            [:.title-keyword {
                :font-weight 700
                :color (str black "!important")
                }]

            [:.text-keyword {
                :font-weight 700
                }]

            [:.table {
                ; here we define the flex for the main table
                :display "flex"
                }
                [:.table-item {
                    :flex 1
                    :margin-right margin
                    }
                    [:&:last-child {
                        :margin-right 0
                        }]
                ]
                ; now we define the flex inside the table-item
                [:.table-item {
                    :display "flex"
                    :flex-flow "column"
                    }
                    [:div { :flex 1}]
                    [:img {
                        :max-width (u/percent 75)
                        :max-height "10vw"
                        :padding-top margin
                        }]
                ]
            ]
        ]

        [:.foot-notes {}
            [:.title {
                :font-family "'oswaldlight'"
                :font-size (u/px 20)
                :padding-bottom margin
                :padding-top margin
                }]
        ]

        [:.picto-lavable:before {
            :content "'E'"
            :font-family "'ecoconsopicto'"
            :color green
            :font-size (u/px 54)
            :vertical-align "bottom"
            :margin-right (u/px 5)
            :line-height (u/px 34)
            }]

        [:.text-picto {
            :display "block"
            :float "none"
            :clear "both"
            :margin-bottom margin
            :margin-top margin
            :min-height (u/px 30)
            }]

        [:.logo-txt {:display "block"}
            [:img {:max-height (u/px 50)}]
        ]

        [:.footer {
                :height (u/px 250)
                :background "url('imgs/footer-back.jpg') center no-repeat"
                :background-size "cover"
                :display "flex"
                :align-items "center"
            }
            [:.footer-text {
                :color "#fff"
                :width  rightW
                :margin-left "35vw"
                :line-height "150%"
                }]
                [:span {:display "block"}]
                [:a {:color "#fff"}]]
    ]

    (media-mobile
        [:body {
            :font-size (u/percent 100)
            }]

        [:ul {}
            [:li {}
                [:&:before {
                    :margin-left (u/px 0)
                    :margin-right (u/px 5)
                    }
                ]
            ]
        ]

        [:ol {} ;initiate the counter
            [:li {:margin-left 0}]
        ]

        [:.content {
            :width "100vw"
            :margin 0
            :padding margin}]

        [:article {
            ; :border-top 0
            :display "block"}]

        [:header {}
            [:.header-top {

                :height (u/px 97)}

                [:.content {
                    :display "flex"
                    :height (u/percent 100)
                    :align-items "center"
                    :justify-content "space-between"
                }

                    [:.logo {
                        :flex 0
                        :margin-right margin}

                        [:img {
                            :height (u/px 45)
                            }]
                        ]
                    [:.slogan {
                        :flex 1
                        ; :text-align "center"
                        }]
                    [:.search {:display "none"}]
                    [:.menu {
                        :flex 0}
                    ]
                ]

                [:.logo {:text-align "initial"}]
            ]

            [:.header-bottom {
                :height "auto"
                :font-size (u/percent 150)}

                [:.content {:display "block"}

                    [:.title {
                        :display "block"
                        :text-align "left"}
                        [:img {:display "none"}]
                    ]
                    [:.sub-title {:display "block"}]
                ]

                [:.title {}]
                [:.sub-title {}]
            ]
        ]

        [:.left-side {
            :width (u/percent 100)
            :text-align "left"
            :margin-bottom margin
            ; :display "none"
            }

            [:.left-number {:display "none"}]

            [:.left-title {
                :font-size (u/px 22)
                ; :display "inline"
                }

                [:&.keyword {
                    ; :font-size "initial"
                    ; :font-weight "initial"
                    ; :display "inline"

                    }
                    [:&:after {
                        :display "none"
                        }
                    ]
                ]
            ]
        ]

        [:.right-side {
            :width (u/percent 100)
            :padding-left margin2
        }

        [:.content-left {
            :width "100%"
            :float "none"
            :left 0
            :margin 0
            :text-align "left"
            :padding-bottom margin
            }]

            [:.title {:font-size (u/px 20)}]
            [:.table {
                ; here we define the flex for the main table
                :display "flex"
                :flex-flow "column"
                }
                [:.table-item {
                    :flex 1
                    :margin-right 0
                    }
                ]
                ; now we define the flex inside the table-item
                [:.table-item {
                    :display "flex"
                    :flex-flow "row"
                    :margin-bottom margin2
                    }
                    [:div { :flex 1}]
                    [:.item-img {:text-align "center"}]
                    [:img {
                        :max-width (u/percent 75)
                        :max-height "25vw"
                        :padding-top 0
                        }]
                ]
            ]
        ]

        [:.foot-notes {}
            [:.title {}]
        ]

        [:.footer {
                ; :height (u/px 250)
                ; :background "url('imgs/footer-back.jpg') center no-repeat"
                ; :background-size "cover"
                ; :display "flex"
                ; :align-items "center"
            }
            [:.footer-text {
                ; :color "#fff"
                :width  "100vw"
                :margin-left 0
                :padding margin
                :text-align "center"
                }]
        ]

        [:.keyword {:font-weight 700}]

        ; [:.picto-lavable:before {
        ;     :content "'E'"
        ;     :font-family "'ecoconsopicto'"
        ;     :color green
        ;     :font-size (u/px 54)
        ;     :vertical-align "bottom"
        ;     :margin-right (u/px 5)
        ;     :line-height (u/px 34)
        ;     }]

        ; [:.text-picto {
        ;     :display "block"
        ;     :float "none"
        ;     :clear "both"
        ;     :margin-bottom margin
        ;     :margin-top margin
        ;     :min-height (u/px 30)
        ;     }]

        ; [:.logo-txt {:display "block"}
        ;     [:img {:max-height (u/px 50)}]
        ; ]
    ) ;end media-mobile

    (css-table/style)
    (css-popup/style)
)



; (defn media-mobile [& rules]
;     "ma fonction pour breakpoints screens"
;     ;(at-media (str "max-width(" mobile-bkp ")") rules)
;     (at-media  [{:max-width (u/px 789) :orientation "portrait"}] rules)
;     )
;
; (defstyles screen
;     [
;         (at-import "reset.css")
;         [:body {
;             :color green
;         }
;         (media-mobile [:div {:width (u/px 600)}])]
;     ])




; EXEMPLE DE FONCTION
; (defn cl
;     ([selector]
;         [(keyword selector) {
;             :width "50%"
;             :float "left"
;             :background-color "#ccc"
;             :display "block"
;             }])
;     ([selector plus]
;         [(keyword selector) (merge {
;             :width "50%"
;             :float "left"
;             :background-color "#ccc"
;             :display "block"
;             } plus)]))
;
; (def def-float {
;     :width "50%"
;     :float "left"
;     :background-color "#ccc"
;     :display "block"
;     })
;
; (defn float-left
;     "this function makes a float rule"
;     ([]
;         def-float)
;     ([extra]
;         (merge def-float extra)))
;
; (defstyles screen
;     [
;         (cl ".content-left" {:width "60px" :height (u/px 600)})
;     ]
; )
