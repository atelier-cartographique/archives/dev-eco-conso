(ns eco-conso.css-table
  (:require
    [garden.def :as gdef]
    [garden.units :as u]
    [garden.color :as color]
    [garden.selectors :refer [after before]]
    [garden.stylesheet :refer [at-import at-media rgb]]

    ))

;blanc
(def blanc (color/rgb 255 255 255))
; vert
(def vert-claire (color/rgb 0x98 0xce 0x1d))
(def vert (color/rgb 0x5e 0xa1 0x29))
(def vert-foncé (color/rgb 94 161 49))
;rouge
(def rouge (color/rgb 215 75 21))



(gdef/defcssfn url
  [unquoted-url]
  (str "\"" unquoted-url "\""))

; widget header
(def type-selector
  [:.table-select {
    :position "relative"
    :display "flex"
    :flex-flow "row"
    :width (u/percent 100)
    :font-size (u/pt (* 12 1))
    ; :margin-bottom (u/px 32)
    }
    [:.select-item-outer {
      :flex "1 1"
      :height (u/px 40)
      :cursor "pointer"
      :margin (str 0 " 10px")
      }

      [:&:first-child {:margin 0}]
      [:&:last-child {:margin 0}]

      [:&.select-item-active {
        :height (u/px 76)
        ; :background-color vert-foncé
        ; :background-size "contain"
        ; :background-position "bottom"
        ; :background-repeat "no-repeat"
        ; :background-image (url "./table-selector-active.png")
        :border-bottom "15px solid transparent"
        :border-image "url(./pictos/table-border-01.png) 22 stretch"
        }
        [:.select-item-inner {
          :background-color rouge
          }]]

      [:.select-item-inner
        {
        :padding-top (u/px 20)
        :height (u/px 61)
        :text-align "center"
        :background-color vert-claire
        :color blanc
        :font-family "'oswaldregular'"
        :line-height "100%"
        :font-size (u/px 20)
        :display "flex"
        :justify-content "space-around"
        }
        [:span {:display "flex"}]
        [:.liste {} [:&:before {:content "'N'" :font-family "'ecoconsopicto'" :font-size "250%" :padding-right (u/px 10)}]]
        [:.diagramme {} [:&:before {:content "'P'" :font-family "'ecoconsopicto'" :font-size "250%" :padding-right (u/px 10)}]]
        [:.tableau {} [:&:before {:content "'O'" :font-family "'ecoconsopicto'" :font-size "250%" :padding-right (u/px 10)}]]
        ]
        (at-media  [{:max-width (u/px 767)}]
          [:.select-item-inner {
            :font-size (u/px 16)
            }])
        (at-media  [{:max-width (u/px 300)}]
        [:.select-item-inner {
          :font-size (u/px 12)
          }])
        ]])

(def diagram [
        [:.row [:span {:display "inline-block" :width "100%" :margin-top "1em"} [:&.value {:text-align "center"}]]]
        [:.table-text-cell {
        :margin-top (u/px 12)
        :margin-bottom (u/px 12)
        ; :display "flex"
        }]
        [:.p-block {
        ;   :margin-top (u/em 1)
        :border-top "1px solid green"
        :padding (str "10px " 0)}
        [:&:last-child {:border-bottom "1px solid green"}]

        [:&.hidden [:.text-bar {
          :width (u/px 0)
          :transition-property "width"
          :transition-duration (u/s 2)
          }]]
          [:&.sized [:.text-bar {
            :transition-property "width"
            :transition-duration (u/s 2)
            }]]
        [:.p-wrap {
          :display "flex"
        }
          [:section
            [:&.section-text {
              ; :flex "2 0 20%"
              :padding-right (u/px 4)
              }]
            [:&.section-bar {
              :flex "0 2 80%"

              }]
          ]
        ]
        [:table {:width (u/percent 100)}
            [:.text-bar {:height (u/em 1)}]
            [:.table-text-cell  {}]
        ]
        ]

        [:.picker {
            :border-bottom "1px solid green"
            :height (u/px 80)
            :display "flex"
            :justify-content "space-between"
            :align-items "center"
        }
            [:.plink {
                :font-family "'news_cycle'"
                :color "lightgrey"
                :display "flex"
                :align-items "center"
                }

                [:&:hover {:cursor "pointer"}]

                [:&:before {
                    :content "'E'"
                    :font-family "'ecoconsopicto'"
                    :font-size "250%"
                    :padding-right (u/px 5)}
                ]

                [:&.highlight {
                    :color "black"
                    }
                    [:&:before {:color vert-claire}]
                ]
            ]
        ]
        [:.ts-head {
                :font-family "'oswaldregular'"
                :border-bottom "1px solid green"
                :display "flex"
                :align-items "center"
                :justify-content "flex-end"
                :padding "10px 0"
                :text-decoration "underline"
            }
            [:span {:flex "0 33%" :text-align "center"}]
        ]

        [:.row {
            :border-bottom "1px solid green"
            :display "flex"
            :align-items "center"
            :justify-content "space-between"
            :padding-bottom (u/px 20)
        }
            [:div {
                :flex "1 66%"
                :display "flex"
                :align-items "center"
                :justify-content "flex-end"}
                [:.value {
                  :flex "0 50%"
                  :text-align "center"
                  :color vert-foncé
                  }]
            ]
            [:.plink {
                :flex "1 33%"
                :font-family "'oswaldregular'"
                :text-decoration "underline"
            }]

        ]

  ])

(def table-diagram [
    [:&.text {}
      [:.picto { :font-family "'eco-conso-lato-picto'" }]
        [:.table-text-cell {:margin 0}]
        [:.text-section {
            :height (u/px 80)
            :display "flex"
            :justify-content "space-between"
            :align-items "center"
            :font-family "'news_cycle'"

        }
          [:.picto {
            :font-family "'eco-conso-lato-picto'"
            }]
          [:.label {
            :text-decoration "underline"
            }]]
        [:.p-block {
            :font-family "'oswaldregular'"}
          [:.p-wrap
            [:.section-text {

              }
              [:.text-text {
                :height (u/px 26)
                  :font-family "eco-conso-lato-picto"
                }]]]
            [:.section-bar {
            }
              [:.table-text-cell {
                  :height (u/px 26)
                  :display "flex"
              }
                [:.text-bar {
                    :height (u/px 6)
                    :margin-top (u/px 9)
                  }]
                [:.picto {
                  :flex "0 0"
                  :margin-left (u/px -3)
                  }]]]
        ]
    ]
])

(def table-list [])
(def table-matrix [
    [:&.matrix {}
        [:.label-box {
            :height (u/px 80)
            :display "flex"
            :flex-direction "row"
            :justify-content "flex-end"
            :align-items "center"
            :font-family "'news_cycle'"
            :text-decoration "underline"
            }
            [:.label {
              :width (u/px 124)
              :text-align "center"
              :padding-left (u/px 12)
              :padding-right (u/px 12)
              }]]
        [:.p-block {
            :font-family "'oswaldregular'"
            ; :text-decoration "underline"
        }
            [:.p-block-row {
                :display "flex"
                :flex-direction "row"
                :justify-content "flex-end"
                :align-items "center"
                ; :text-decoration "initial!important"
                ; :font-family "'news_cycle'"
                :padding-top (u/px 5)
                :color vert-foncé
                }
                [:.table-cell {
                  :font-family "eco-conso-lato-picto"
                  :width (u/px 124)
                  :text-align "center"
                  :padding-left (u/px 12)
                  :padding-right (u/px 12)
                  }]]
        ]
    ]
])

(defn style
  []
  [:.eco-table {
    ; :margin "auto"
    ; :max-width (u/px 482)
  }
    [:span.value {

        :font-family "eco-conso-lato-picto"
      }]
    type-selector
    diagram
    table-diagram
    table-list
    table-matrix])
