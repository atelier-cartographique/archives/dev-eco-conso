(ns eco-conso.subs
    (:require-macros [reagent.ratom :refer [reaction]])
    (:require [re-frame.core :as re-frame]))


(re-frame/reg-sub
 :table/type
 (fn [db [_ table-id]]
   (get-in db [:components :table table-id :type])))

(re-frame/reg-sub
  :table/name
  (fn [db [_ table-id]]
    (get-in db [:components :table table-id :name])))

(re-frame/reg-sub
  :table/data
  (fn [db [_ table-id]]
    (get-in db [:components :table table-id :data])))

(re-frame/reg-sub
  :table/text-geom
  (fn [db [_ table-id q]]
    (get-in db [:components :table table-id :text q])))


;; popup
(re-frame/reg-sub
  :popup/active
  (fn [db _]
    (println "popup/active" (get-in db [:components :popup :active]) )
    (get-in db [:components :popup :active])
  )
)

(re-frame/reg-sub
  :popup/data
  (fn [db [_ uid]]
    (println "popup/data" uid)
    (println (get-in db [:components :popup]))
    (get-in db [:components :popup uid])))
