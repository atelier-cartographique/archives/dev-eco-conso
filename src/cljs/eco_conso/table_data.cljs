(ns eco-conso.table-data)

(defrecord named-rec [id name])
(defrecord data-value [value label])

(defn- sv [label] (data-value. (count label) label))
(defn- dv [value label] (data-value. value label))

(defn bool? [x]
  (or (true? x) (false? x)))

;comparaison

(def langes-comparaison-parameters [
  (named-rec. 1 "Production de déchets")
  (named-rec. 2 "Équivalence en production de CO₂")
  (named-rec. 3 "Consommation d'eau")
  (named-rec. 4 "Quantité de couches")
  (named-rec. 5 "Coût")])

(def langes-comparaison-products [
  (named-rec. :lavable "Lange lavable")
  (named-rec. :jetable "Lange jetable")])

(def langes-comparaison-matrix-in [
  [:lavable
    [1 "284kg" 284]
    [2 "342kg à 479kg CO₂" 419.5]
    [3 "85964 litres" 85964]
    [4 "Entre 24 et 40" 32]
    [5 "de 490€ à 740€" 615]]

  [:jetable
    [1 "900kg" 900]
    [2 "550kg CO₂" 550]
    [3 "34081 litres" 34081]
    [4 "4500" 4500]
    [5 "de 925€ à 2250€" 1587.5]]

; sur 2,5 années par enfant

])
;matière

(def langes-matiere-parameters [

  (named-rec. 1 "Impact environnemental")
  (named-rec. 2 "Absorbance")
  (named-rec. 3 "Propriétés bactériostatiques naturelles")
  (named-rec. 4 "Rapidité de séchage")
  (named-rec. 5 "Résistance aux séchages répétitifs")
  (named-rec. 6 "Coût")])

(def langes-matiere-products [
  (named-rec. :coton-bio "Coton bio")
  (named-rec. :bambou "Bambou")
  (named-rec. :chanvre "Chanvre")
  (named-rec. :microfibre "Microfibre")])

(def langes-matiere-matrix-in [
  [:coton-bio
    [1 "☿☿"]
    [2 "☿"]
    [3 "non" false]
    [4 "☿☿"]
    [5 "☿☿"]
    [6 "♇"]]

  [:bambou
    [1 "☿"]
    [2 "☿☿☿"]
    [3 "oui" true]
    [4 "☿"]
    [5 "☿☿☿"]
    [6 "♇"]]

  [:chanvre
    [1 "☿☿☿"]
    [2 "☿☿"]
    [3 "oui" true]
    [4 "☿"]
    [5 "☿☿☿"]
    [6 "♇♇♇"]]

  [:microfibre
    [1 "☿☿"]
    [2 "☿☿☿"]
    [3 "oui" true]
    [4 "☿"]
    [5 "☿☿☿"]
    [6 "♇♇♇"]]
])

;culotte protection

(def langes-protection-parameters [
  (named-rec. 1 "Impact environnemental")
  (named-rec. 2 "Imperméabilité")
  (named-rec. 3 "Respiration")
  (named-rec. 4 "Rapidité de séchage")])

(def langes-protection-products [
  (named-rec. :pul "PUL PolyUréthane Laminé")
  (named-rec. :polaire "Polaire")
  (named-rec. :laine "Laine")])

(def langes-protection-matrix-in [
  [:pul
    [1 "☿☿"]
    [2 "☿☿☿"]
    [3 "☿☿"]
    [4 "☿☿☿"]]

  [:polaire
    [1 "☿☿"]
    [2 "☿☿"]
    [3 "☿☿☿"]
    [4 "☿"]]

  [:laine
    [1 "☿☿☿"]
    [2 "☿☿ si bain de lanoline tous les mois"]
    [3 "☿☿☿"]
    [4 "☿"]]

])

;système

(def langes-systeme-parameters [
  (named-rec. 1 "Description")
  (named-rec. 2 "Adaptation aux nouveaux-nés")
  (named-rec. 3 "Simplicité d'utilisation")
  (named-rec. 4 "Protection contre les fuites")
  (named-rec. 5 "Nettoyage systématique de la culotte de protection")
  (named-rec. 6 "Rapidité de séchage")
  (named-rec. 7 "Coût")])

(def langes-systeme-products [
  (named-rec. :lnpc "Lange non préformé ☿ culotte")
  (named-rec. :lpc "Lange préformé ☿ culotte")
  (named-rec. :te1 "Lange « tout en un » (TE1) ")
  (named-rec. :te1p "Lange « tout en un » (TE1) avec poche")
  (named-rec. :te2 "Lange « tout en deux » (TE2)")
  (named-rec. :te3 "Lange « tout en trois » (TE3)")])

(def langes-systeme-matrix-in [
  [:lnpc
    [1 "Le lange est un tissu à nouer ou à fermer avec une attache (« snappi »). Ajouter un tissu absorbant à l'intérieur. À surmonter d'une culotte de protection."]
    [2 "☿☿☿"]
    [3 "☿"]
    [4 "☿☿"]
    [5 "non" false]
    [6 "☿☿☿"]
    [7 "€"]]

  [:lpc
    [1 "Le lange est préformé et s'attache avec des pressions, des velcros ou plus rarement des lacets. À surmonter d'une culotte de protection."]
    [2 "☿☿"]
    [3 "☿☿"]
    [4 "☿☿☿"]
    [5 "non" false]
    [6 "☿☿☿"]
    [7 "€€"]]

  [:te1
    [1 "Le lange préformé est cousu à la culotte de protection."]
    [2 "☿☿"]
    [3 "☿☿☿"]
    [4 "☿☿☿"]
    [5 "oui" true]
    [6 "☿"]
    [7 "€€"]]

  [:te1p
    [1 "Le lange est préformé et comporte une partie intérieure cousue à une culotte de protection. Une poche est présente dans la partie intérieure afin d'y glisser des inserts."]
    [2 "☿☿"]
    [3 "☿ nécessité d'insérer et d'enlever les inserts à chaque lavage"]
    [4 "☿☿☿"]
    [5 "oui" true]
    [6 "☿☿"]
    [7 "€€"]]

  [:te2
    [1 "Les couches absorbantes ne sont pas cousues à la culotte de protection mais attachée grâce à des boutons pressions."]
    [2 "☿☿"]
    [3 "☿☿☿"]
    [4 "☿☿☿"]
    [5 "non" false]
    [6 "☿☿☿"]
    [7 "€€€"]]

  [:te3
    [1 "La culotte de protection n’est pas imperméable contient une nacelle imperméable et détachable. Cette nacelle contient à son tour un absorbant lavable."]
    [2 "☿☿"]
    [3 "☿☿☿"]
    [4 "☿☿☿"]
    [5 "non ni pour la culotte, ni pour la nacelle false" false]
    [6 "☿☿☿"]
    [7 "€€€"]]

])

;modèle

(def langes-modele-parameters [
  (named-rec. 1 "Durée d'utilisation")
  (named-rec. 2 "Résistance au cours du temps")
  (named-rec. 3 "Adaptation à la morphologie de l'enfant")
  (named-rec. 4 "Protection contre les fuites")
  (named-rec. 5 "Coût global")])

(def langes-modele-products [
  (named-rec. :fixe "En différentes tailles fixes")
  (named-rec. :unique "« Taille unique » avec pressions ou velcros pour ajuster à la morphologie")])

(def langes-modele-matrix-in [
  [:fixe
    [1 "Courte pour chaque taille (mais utilisable pour d'autres enfants)."]
    [2 "☿"]
    [3 "☿"]
    [4 "☿"]
    [5 "€€€"]]

  [:unique
    [1 "Longue (pour les enfants de 4 à 15kg environ)."]
    [2 "-"]
    [3 "-"]
    [4 "-"]
    [5 "€€"]]

])


;sèche-linge

;comparaison

(def seche-comparaison-parameters [
  (named-rec. 1 "Prix achat")
  (named-rec. 2 "Classe énergétique")
  (named-rec. 3 "Conso kWh/an")
  (named-rec. 4 "Conso €/an")
  (named-rec. 5 "Coût total sur 10 ans en €")
  (named-rec. 6 "Économie annuelle sur 10 ans")])

(def seche-comparaison-products [
  (named-rec. :evacuation "Évacuation")
  (named-rec. :condensation "Condensation")
  (named-rec. :pompe "Pompe à chaleur")])

(def seche-comparaison-matrix-in [
  [:evacuation
    [1 "449€" 449]
    [2 "C"]
    [3 "512kWh" 512]
    [4 "128€" 128]
    [5 "1729€" 1729]
    [6 "0€" 0]]

  [:condensation
    [1 "479€" 479]
    [2 "B"]
    [3 "504kWh" 504]
    [4 "126€" 126]
    [5 "1739€" 1739]
    [6 "0€" 0]]

  [:pompe
    [1 "549€" 549]
    [2 "A+"]
    [3 "260kWh" 260]
    [4 "64,5€ " 64.5]
    [5 "1194€" 1194]
    [6 "54€" 54]]

])

;prolonger

(def seche-prolonger-parameters [
  (named-rec. 1 "% du prix d'achat à ne pas dépasser pour que la réparation vaille la peine")])

(def seche-prolonger-products [
  (named-rec. :trois "de 3 à 4 ans")
  (named-rec. :cinq "de 5 à 7 ans")
  (named-rec. :huit "de 8 à 10 ans")
  (named-rec. :dix "plus de 10 ans")])

(def seche-prolonger-matrix-in [
  [:trois
    [1 "50%" 50]]

  [:cinq
    [1 "35%" 35]]

  [:huit
    [1 "15%" 15]]

  [:dix
    [1 "5%" 5]]

])

(defn make-row
  ([product parameter label]
    {:product product :parameter parameter :value (sv label)})
  ([product parameter label value]
    {:product product :parameter parameter :value (dv value label)}))

(defn make-col
  [col]
  (let [product (first col)
        records (rest col)
        mkrow (partial make-row product)]
    (mapv (fn [row]
      (let [record (apply mkrow row)]
      ; (println record)
      record)) records)))

(defn in->out [matrix-in]
  (apply concat (mapv make-col matrix-in)))


(def langes-comparaison (in->out  langes-comparaison-matrix-in))
(def langes-matiere (in->out  langes-matiere-matrix-in))
(def langes-systeme (in->out  langes-systeme-matrix-in))
(def langes-modele (in->out  langes-modele-matrix-in))
(def seche-comparaison (in->out  seche-comparaison-matrix-in))
(def seche-prolonger (in->out  seche-prolonger-matrix-in))

(defn mk-table [matrix products parameters]
  (let [
    ; matrix (cljs/var-get (symbol (str "table-data/" name)))
    ; products (cljs/var-get (symbol (str "table-data/" name "-products")))
    ; parameters (cljs/var-get (symbol (str "table-data/" name "-parameters")))
    key {:type :product :product-id [(:id (first products))]}
    ]
    {
      :type :matrix
      :data {
        :key key
        :matrix matrix
        :products products
        :parameters parameters
      }
      }))


(def tables {
  "langes-comparaison" (mk-table langes-comparaison langes-comparaison-products langes-comparaison-parameters)
  "langes-matiere" (mk-table langes-matiere langes-matiere-products langes-matiere-parameters)
  "langes-systeme" (mk-table langes-systeme langes-systeme-products langes-systeme-parameters)
  "langes-modele" (mk-table langes-modele langes-modele-products langes-modele-parameters)
  "seche-prolonger" (mk-table seche-prolonger seche-prolonger-products seche-prolonger-parameters)
  "seche-comparaison" (mk-table seche-comparaison seche-comparaison-products seche-comparaison-parameters)
  })
