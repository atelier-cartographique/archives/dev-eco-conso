(ns eco-conso.handlers
  (:require [clojure.string :as str]
            [re-frame.core :as re-frame]
            [eco-conso.db :as eco-db]
              ; [eco-conso.http-fx]
            [eco-conso.popup-data :as popup-data]
            [eco-conso.table-data :as table-data]))

(re-frame/reg-event-db
 :initialize-db
 (fn  [_ _]
   eco-db/default-db))

(defn merge-id [ids id]
  (let [
    new-vec (conj ids id)
    rev-vec (rseq new-vec)
    t2 (take 2 rev-vec)
    rt2 (rseq (apply vector t2))]
(println "merge-id")
    (apply vector rt2)))

(re-frame/reg-event-db
 :table-set-key
 (fn [db [_ table-id k]]
   (let [cur-key (get-in db [:components :table table-id :data :key])
         cur-type (:type cur-key)]

     (if (= cur-type (:type k))
       (let [
         cur-ids (if (= :product cur-type) (:product-id cur-key) (:parameter-id cur-key))
         k-id (if (= :product cur-type) (:product-id k) (:parameter-id k))
         new-ids (merge-id cur-ids k-id)
         new-k (if (= :product cur-type) (assoc k :product-id new-ids) (assoc k :parameter-id new-ids))]

         (assoc-in db [:components :table table-id :data :key] new-k))
       (let [
         new-k (if (= :product (:type k)) (assoc k :product-id [(:product-id k)]) (assoc k :parameter-id [(:parameter-id k)]))]

         (assoc-in db [:components :table table-id :data :key] new-k))))))

(re-frame/reg-event-db
 :table-set-type
 (fn [db [_ table-id t]]
   (assoc-in db [:components :table table-id :type] t)))

(re-frame/reg-event-db
 :table-text-set-cell-geometry
 (fn [db [_ table-id cell-id geometry]]
   (assoc-in db [:components :table table-id :text cell-id] geometry)))

(defn find-row
  [path val rows]
  (let [results (filter (fn [row] (= val (get-in row path))) rows)]
    ; (println results)
    (first results)))

(defn px [i] (str (int i) "px"))

(defn nodelist-to-seq
  "Converts nodelist to (not lazy) seq."
  [nl]
  (let [result-seq (map #(.item nl %) (range (.-length nl)))]
    (doall result-seq)))

;
(def text-margin 32)
(defn adjust-cell-text [mm rows block-element]

  (let [row-max (find-row [:value :value] (:max mm) rows)
        selector-max (str "[data-product=\"" (name (:product row-max)) "\"]")
        cell-max (.querySelector block-element selector-max)
        cells (nodelist-to-seq (.querySelectorAll block-element ".table-text-cell"))]

    (doseq [cell cells]
      (let [product-id (.getAttribute cell "data-product")
            product-row (find-row [:value :value] (:max mm) rows)
            row (find-row [:product] (keyword product-id) rows)
            bar-width-base  (/ (* (:value (:value row)) 100) (:max mm))
            bar-width (max 0 (- bar-width-base text-margin))
            left (+ bar-width text-margin)]
         (re-frame/dispatch []
                            :table-text-set-cell-geometry
                            row
                            {:bar-width bar-width
            ; :text-width text-width
            ; :left left
                             :top 0})))))

(re-frame/reg-event-db
 :adjust-cell-text
 (fn [db [_ mm rows block-element]]
   (when-not (= nil block-element) (adjust-cell-text mm rows block-element))
   db))


;; popup

; (re-frame/reg-event-db
;  :popup/init
;  (fn [db [_ uid label]]
;    (let [p-data {:body ((keyword label) popup-data/data)}]
;      (assoc-in db [:components :popup uid] p-data))))


(re-frame/reg-event-db
 :popup/open
 (fn [db [_ uid popup-key]]
   (let [
     db-with-active (assoc-in db [:components :popup :active] uid)
     popup {:body ((keyword popup-key) popup-data/data)}]
     (println "open" uid popup)
     (assoc-in db-with-active [:components :popup uid] popup))))

(re-frame/reg-event-db
 :popup/close
 (fn [db _]
   (assoc-in db [:components :popup :active] nil)))
