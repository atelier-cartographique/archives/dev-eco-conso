(ns eco-conso.db
  (:require
    [cljs.core :as cljs]
    [eco-conso.table-data :as table-data]))



(def default-db
  {
    ;; components are in type collections keyed by uuid
    :components {
      :table table-data/tables
      :popup {}
    }
  }
)
; (println  default-db)
