(ns eco-conso.popup-data)


(def data {
  :GOTS [:section [:h1 "Global Organic Textile Standard"]
            [:p "Seuls les produits textiles contenant un minimum de 70 % de fibres biologiques peuvent être certifiés GOTS."]
            [:ul
              [:li "Les intrants chimiques utilisés, comme les colorants et adjuvants doivent être conformes à un certain nombre de critères environnementaux et toxicologiques."]
              [:li "Les systèmes de production et de transformation doivent également être conformes aux règles environnementales."]
              [:li "Une installation de traitement des eaux usées fonctionnelle est obligatoire pour toute unité de traitement humide utilisée et tous les industriels doivent se conformer à un certain nombre de critères sociaux."]
              [:li "Les principaux critères du référentiel GOTS, son système d'assurance qualité et ses principes de revue de procédure se trouvent résumés sous cette rubrique."]]]


;sèche linge raison 2

  :ENERTECH [:section [:h1 "Enertech"]
            [:p "C´est en 1979 que l´entreprise ENERTEC voit le jour, dans les années qui ont suivi directement le premier choc pétrolier. Dans son activité de base, l’installation de chauffage, sanitaire et climatisation, ENERTEC veut promouvoir l´économie, et le développement des sources renouvelables d´énergie."]]


;séche linge raison 2 à condensation

  :CLASSES [:section [:h1 "Classes énergie"]
            [:p "L'étiquette-énergie est une fiche destinée au consommateur qui résume les caractéristiques d'un produit, en particulier ses performances énergétiques, afin de faciliter le choix entre différents modèles. L'efficacité énergétique de l'appareil est évaluée en termes de classes d'efficacité énergétique notées de A+++ à D ou G. La classe A+++ est celle au rendement optimal, G la moins efficace. Cependant toutes les catégories d'appareil ne comportent pas encore les classes A+ à A+++."]]


  :OEKO [:section [:h1 "OEKO-TEX® Standard 100"]
            [:p "L'OEKO-TEX® Standard 100 est un système de contrôle et de certification indépendant pour les produits textiles bruts, intermédiaires et finis à toutes les étapes de traitement. Exemples pour des articles certifiables : fil à coudre brut et teint/transformé, tissus et tricots bruts et teints/transformés, articles manufacturés (vêtement de tout type, textiles d'intérieur, draps, articles en éponge, jouets textiles (et bien d'autres). Les contrôles de substances nocives comprennent :"]
            [:ul
              [:li "des substances interdites par la loi"]
              [:li "des substances réglementées par la loi"]
              [:li "des produits chimiques notoirement susceptibles de nuire à la santé (mais toutefois pas encore réglementés par la loi)"]
              [:li "ainsi que paramètres pour la prévention en matière de santé"]]]
  })
