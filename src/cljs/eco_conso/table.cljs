(ns eco-conso.table
    (:require [cljs.core :as cljs]
              [reagent.core :as reagent]
              [re-frame.core :as re-frame]
              [devtools.core :as devtools]
              [eco-conso.handlers]
              [eco-conso.subs]
              [cljs.pprint :refer [pprint]]
              ; [eco-conso.views :as views]
              [eco-conso.config :as config]
              [clairvoyant.core :refer-macros [trace-forms]]
              [re-frame-tracer.core :refer [tracer]]))

(defmulti filter-product (fn [pid _] (if (vector? pid) :filter-multi :filter-single)))

(defmethod filter-product :filter-single [pid matrix]
  (filter
    (fn [row]
      (= pid (:product row)))
    matrix))

(defmethod filter-product :filter-multi [pids matrix]
  (filter
    (fn [row]
      (let [rowpid (:product row)]
        (some #(= rowpid %) pids)
      ))
    matrix))

(defmulti filter-parameter (fn [pid _] (if (vector? pid) :filter-multi :filter-single)))

(defmethod filter-parameter :filter-single [pid matrix]
  (filter #(= pid (:parameter %)) matrix))

(defmethod filter-parameter :filter-multi [pids matrix]
  (filter
    (fn [row]
      (let [rowpid (:parameter row)]
        (some #(= rowpid %) pids)
      ))
    matrix))


(defn get-rec [id records]
  (first (filter #(= id (:id %)) records)))

(defn get-name [id records]
  (:name (get-rec id records)))

(defn map-with
  ([tag attrs f v]
    (into [tag attrs] (mapv (fn [item] (f item)) v)))
  ([tag f v]
    (into [tag] (mapv (fn [item] (f item)) v))))




(defn set-product-k
  [table-name p]
  (let [pid (:id p)
        k {:type :product :product-id pid}]
    (fn [] (re-frame/dispatch [:table-set-key table-name k]))))

(defn set-parameter-k
  [table-name p]
  (let [pid (:id p)
        k {:type :parameter :parameter-id pid}]
    (fn [] (re-frame/dispatch [:table-set-key table-name k]))))

(defn parameter-link [table-name pids]
  (fn [p]
    (let [name (:name p)
        id (:id p)
        highlight (some #(= id %) pids)
        klass (if highlight "plink parameter highlight" "plink parameter")]
    [:span {:class klass :key (str "pl-" id) :on-click (set-parameter-k table-name p)} (str " " name " ")]
  )))

(defn product-link [table-name pids]
  (fn [p]
    (let [name (:name p)
          id (:id p)
          highlight (some #(= id %) pids)
          klass (if highlight "plink parameter highlight" "plink parameter")]
      [:span {:class klass :key (str "pl-" id) :on-click (set-product-k table-name p)} (str " " name " ")]
    )))

(defn product-picker [name t pids]
  (into [:div {:class "picker"}] (mapv (product-link name pids) (:products t) )))

(defn parameter-picker [name t pids]
  (into [:div {:class "picker"}] (mapv (parameter-link name pids) (:parameters t))))



(defn row-product-value [matrix parameter pids]
  (let [
      sub-mat (filter-parameter parameter matrix)
    ]
    (mapv (fn [pid]
      (let [rec (first (filter-product pid sub-mat))]
      ; (println pid rec)
        [:span {:class "value"}  (:label (:value rec))])) pids)))


(defn row-parameter-value [matrix product pids]
  (let [
      sub-mat (filter-product product matrix)
    ]
    (mapv (fn [pid]
      (let [rec (first (filter-parameter pid sub-mat))]
      ; (println pid rec)
        [:span {:class "value"}  (:label (:value rec))])) pids)))


(defmulti table-select (fn [_ t]
   (get-in @t [:key :type])))

(defmethod table-select :product [name t]
  (println "table-select :product" name)
  (let [table @t
        matrix (:matrix table)
        products (:products table)
        parameters (:parameters table)
        pids (get-in table [:key :product-id])
        pid (first pids)]
      [:div
          [product-picker name table pids]
          (map-with :div {:class "ts-head"} (fn [ppid]
            [:span (str " " (:name (get-rec ppid products)) " ")]) pids)

          (map
            (fn [rec]
              (let [
                pl ((parameter-link name [])(get-rec (:parameter rec) parameters))
                rpv (into [:div] (row-product-value matrix (:parameter rec) pids))
                ]
              [:div {:key (str "param-" pid "-" (:parameter rec)) :class "row"}
                pl
                rpv
                ]))
            (filter-product  pid  matrix))
        ]
  )
)

(defmethod table-select :parameter [name t]
  (println "table-select :parameter" name)
  (let [table @t
        matrix (:matrix table)
        products (:products table)
        parameters (:parameters table)
        pids (get-in table [:key :parameter-id])
        pid (first pids)]

    [:div
      [parameter-picker name table pids]
      (map
        (fn [rec]
          (let [
            pl ((product-link name [])(get-rec (:product rec) products))
            rpv (into [:div] (row-parameter-value matrix (:product rec) pids))
            ]
          [:div {:key (str "param-" pid "-" (:product rec)) :class "row"}
            pl
            rpv]))
        (filter-parameter  pid  matrix))
    ]
  )
)

(defmethod table-select :default [t]
  [:div "empty table"])


;; table-matrix


(defn pp-cell-matrix [matrix parameter product]
  (let [fp (filter-product (:id product) matrix)
        row (first (filter-parameter (:id parameter) fp))]
        ; (println (:name parameter) (:name product) row)
        [:div {:class "table-cell lato-picto"} (:label (:value row))]))

(defn param-block-matrix [matrix products parameter]
  (let [pc (partial pp-cell-matrix matrix parameter)]
    [:div {:class "p-block"} (:name parameter)
      (map-with :div {:class "p-block-row"} pc products)]))

(defn table-matrix [t]
  (let [table @t
        matrix (:matrix table)
        products (:products table)
        parameters (:parameters table)
        pb (partial param-block-matrix matrix products)]

      [:div
        (map-with :div {:class "label-box"} (fn [p] [:div {:class "label"} (:name p)]) products)
        (map-with :div {:class "rows"} pb parameters)
      ]
    ))

;; table-text
(defn px [i] (str (int i) "px"))
(defn percent [i] (str (int i) "%"))
(def colors [
  "#98c21d"
  "#ef7b10"
  "#fdc300"
  "#8c3a8e"
  "#d7490c"
  "#82c3ec"
  "#d72d80"
  ])
(def pictos [ "♖" "♞" "♢" "♦" "♕" "♙" "♝" "♡"])

(defn colorize [products]
  (mapv (fn [p c picto] (assoc p :color c :picto picto)) products colors pictos))

(defn span
  ([label] [:span (str " " label " ")])
  ([attrs label] [:span attrs (str " " label " ")]))

(defn min&max-val [m]
  (let [vals (mapv (fn [r] (get-in r [:value :value])) m)
        min-val (apply min vals)
        max-val (apply max vals)]
        {:min min-val :max max-val}))

(defn dispatch-adjust [minmax par-select elem]
  (re-frame/dispatch [:adjust-cell-text minmax par-select elem]))

(defn param-block-text-bar [par-select parameter product]
  (let [
        row (first (filter-product (:id product) par-select))
        mm (min&max-val par-select)
        bar-width (/ (* 100 (get-in row [:value :value])) (:max mm))
        ; geometry-atom (re-frame/subscribe [:table/text-geom row])
        ]

          (fn [par-select parameter product]
            (let [
              ; geometry @geometry-atom
              color (:color parameter)
              picto (:picto product)
              klass "table-text-cell sized"
              bar-style {
                  :background-color color
                  :width (percent bar-width)
                }
              ]

            [:div {
              ; :key key
              ; :data-product (name (:id product))
              :class klass
              }
              [:div {:class "text-bar" :style bar-style}]
              [:div {:class "picto"} picto]
              ]
          ))
  )
)



(defn param-block-text-text [par-select parameter product]
  (let [row (first (filter-product (:id product) par-select))]
    (fn [par-select parameter product]
      [:div {:class "text-text"} (:label (:value row))]
    )))

(defn param-block-text [matrix products parameter]
  (let [par-select (filter-parameter (:id parameter) matrix)
        ; dispatcher (partial dispatch-adjust (min&max-val par-select) par-select)
        ]
    (fn [matrix products parameter]
      [:div {
        :class "p-block"
        ; :ref dispatcher
        } (:name parameter)
        [:div {:class "p-wrap"}
        (map-with :section {:class "section-text"}
          (fn [product]
            [param-block-text-text par-select parameter product]) products)
        (map-with :section {:class "section-bar"}
          (fn [product]
            [param-block-text-bar par-select parameter product]) products)
        ]
      ]
    )
  )
)

(defn table-text [t]
  (let [table @t
        matrix (:matrix table)
        products (colorize (:products table))
        parameters (colorize (:parameters table))
        comp [:div
          (map-with :div {:class "text-section"}
            (fn [p] [:div
                  (span {:class "picto"} (str (:picto p) " "))
                  (span {:class "label"}  (:name p))]) products)

          ; (map-with :div {:class "rows"} pb parameters)
          (map-with :div {:class "rows"}
            (fn [parameter]
              [param-block-text matrix products parameter]) parameters)
        ]]
      ; (println comp)
      comp
    )
)


;; TOP LEVEL  TABLE -----------------------------------------

(defmulti table (fn [_ type] type))

(defmethod table :select [name tt]
  (let [data (re-frame/subscribe [:table/data name])]
    [table-select name data]))
    ; (println "foo")
    ; (table-select name data)))

(defmethod table :matrix [name tt]
  (let [data (re-frame/subscribe [:table/data name])]
    [table-matrix  data]))

(defmethod table :text [name tt]
  (let [data (re-frame/subscribe [:table/data name])]
    [table-text  data]))

(defn wrap
  ([tag block] [tag block])
  ([tag class block] [tag {:class class} block]))

; add-class-if
(defn aci [c base-class class]
  (if c (str base-class " " class) base-class))

; prep-equals
(defn pe [left-member]
  (fn [right-member] (= left-member right-member)))

(defn type-selector [name type]
  (let [p (pe type)]
    [:div {:class "table-select"}
    (wrap :div (aci (p :matrix) "select-item-outer" "select-item-active")
          (wrap :div "select-item-inner" [:span {:class "liste" :on-click #(re-frame/dispatch [:table-set-type name :matrix])} " Comparer en chiffres "]))

        (wrap :div (aci (p :text) "select-item-outer" "select-item-active")
          (wrap :div "select-item-inner" [:span {:class "diagramme" :on-click #(re-frame/dispatch [:table-set-type name :text])} " Comparer en schéma "]))

        (wrap :div (aci (p :select) "select-item-outer" "select-item-active")
   (wrap :div "select-item-inner" [:span {:class "tableau" :on-click #(re-frame/dispatch [:table-set-type name :select])} " Comparer en duel "]))
    ]))


(defn mount-table [name]
  (let [type (re-frame/subscribe [:table/type name])]
    (fn []
      ; (println name @type)
      [:div
        [:div {:class (str "eco-table "  (cljs/name @type))}
          [type-selector name @type]
          [table name @type]
        ]
      ])))

(defn nodelist-to-seq
  "Converts nodelist to (not lazy) seq."
  [nl]
  (let [result-seq (map #(.item nl %) (range (.-length nl)))]
    (doall result-seq)))


(defn mount []
 ; attach tables to [data-table]
 (let [elements (nodelist-to-seq (.querySelectorAll js/document "[data-table]"))]
 (doseq [element elements]
   (let [name (.getAttribute element "data-table")]
     (reagent/render [mount-table name] element)))))
