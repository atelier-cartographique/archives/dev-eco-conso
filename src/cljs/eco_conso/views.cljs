(ns eco-conso.views
    (:require [re-frame.core :as re-frame]))


;; search

(defn dispatch-input-value
  "Is a function which returns a function
   which dispatches input valu to keyed handler.
   "
  [key]
    (fn [event]
      (re-frame/dispatch [key (-> event .-target .-value)])))

(defn search []
  (let [term (re-frame/subscribe [:search-results])]
    (fn []
      [:div {:class "search"}
        [:input {
          :on-change (dispatch-input-value :set-search-term)}]
        [:div @term]])))


;; header

(defn header []
  [:div {:class "header"}
    [:a {:href "/"} "écoconso"]
    [search nil]])

;; home

(defn home-panel []
  (let [name (re-frame/subscribe [:name])]
    (fn []
      [:div (str "Hello from " @name ". This is the Home Page.")
       [:div [:a {:href "#/about"} "go to About Page"]]])))


;; about

(defn about-panel []
  (fn []
    [:div "This is the About Page."
     [:div [:a {:href "#/"} "go to Home Page"]]]))


;; main

(defmulti panels identity)
(defmethod panels :home-panel [] [home-panel])
(defmethod panels :about-panel [] [about-panel])
(defmethod panels :default [] [:div])

(defn show-panel
  [panel-name]
  [panels panel-name])

(defn main-panel []
  (let [active-panel (re-frame/subscribe [:active-panel])]
    (println "in main-panel????")
    (fn []
      [:div
        [header nil]
        [show-panel @active-panel]])))
