(ns eco-conso.popup
    (:require [reagent.core :as reagent]
              [re-frame.core :as re-frame]
              [devtools.core :as devtools]
              [eco-conso.handlers]
              [eco-conso.subs]
              [cljs.pprint :refer [pprint]]
              [eco-conso.config :as config]
              [clairvoyant.core :refer-macros [trace-forms]]
              [re-frame-tracer.core :refer [tracer]]
              [cljs-uuid-utils.core :as uuid]))


(defn nodelist-to-seq
  "Converts nodelist to (not lazy) seq."
  [nl]
  (let [result-seq (map #(.item nl %) (range (.-length nl)))]
    (doall result-seq)))


(defn popup-view-inner []
  (let [active (re-frame/subscribe [:popup/active])]
    (fn []
      (let [
        uid @active
        popup-data (re-frame/subscribe [:popup/data uid])
        close (fn [] (re-frame/dispatch [:popup/close]))
        ]
      ; (println "popup-view-inner" @popup-data)
        (if-not uid
          [:span]
          ; else
          [:div {:class "popup-mask"  :on-click close}
            [:div {:class "popup-inner"}
              [:div {:class "popup-close" :on-click close} "x"]
              (:body @popup-data)]])))))

(defn popup-view []
      [popup-view-inner])


(defn popup-trigger-handler [uid label]
  (fn [_]
    (re-frame/dispatch [:popup/open uid label])))

(defn popup-trigger [element label]
  (let [unique-id (uuid/make-random-uuid)
        handler (popup-trigger-handler unique-id label)]
    ; (re-frame/dispatch [:popup/init unique-id label])
    (.addEventListener element "click" handler false)))


(defn elem-by-id [id]
  (let [element (.getElementById js/document id)]
    (if element
      (println "found id" id element)
      (println "did not find id" id))
    element))

(defn mount []

  ; attach to id="popup-view"
  (reagent/render [popup-view] (elem-by-id "popup-view"))

  ; attach click handler to [data-popup]
  (let [elements (nodelist-to-seq (.querySelectorAll js/document "[data-popup]"))]
  (doseq [element elements]
    (let [label (.getAttribute element "data-popup")]
      (popup-trigger element label))))

      )
