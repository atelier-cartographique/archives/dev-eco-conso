(ns eco-conso.core
    (:require [reagent.core :as reagent]
              [re-frame.core :as re-frame]
              [devtools.core :as devtools]
              [eco-conso.handlers]
              [eco-conso.subs]
              ; [eco-conso.routes :as routes]
              ; [eco-conso.views :as views]
              [eco-conso.config :as config]
              [eco_conso.table :as table]
              [eco_conso.popup :as popup]))




(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (devtools/install!)))

; (defn mount-root []
;   (reagent/render [views/main-panel]
;                   (.getElementById js/document "app")))



(defn ^:export init []
  ; (routes/app-routes)
  (re-frame/dispatch-sync [:initialize-db])
  (dev-setup)
  (popup/mount)
  (table/mount)
  )
